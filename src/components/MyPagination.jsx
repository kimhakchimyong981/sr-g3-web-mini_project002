import React from 'react'
import {Container, Pagination} from 'react-bootstrap'
export default function MyPagination() {
    return (
        <Container>
            <Pagination style={{marginTop: 10}}>
                <Pagination.First />
                <Pagination.Item>{1}</Pagination.Item>
                <Pagination.Item>{2}</Pagination.Item>
                <Pagination.Item>{3}</Pagination.Item>
                <Pagination.Last />
            </Pagination>
        </Container>
    )
}
