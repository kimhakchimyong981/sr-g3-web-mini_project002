import React from 'react'
import { useState } from 'react'
import Moment from 'react-moment';
import { Card, DropdownButton, ButtonGroup, Button, Dropdown, Row, Container, Modal, Col } from 'react-bootstrap'
export default function MyCard({ person, onDelete }) {
    const [modalIsopen, setModal] = useState(false)
    return (
        <Container>
            <Row>
                <Col xs="12" style={{ display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "flex_Start" }}>
                    {person.map((item, index) => (
                        <Card key={index} style={{ width: 300, margin: "10px" }} >
                            <Card.Header style={{ textAlign: 'center' }}>
                                <DropdownButton as={ButtonGroup} title="Action" id="bg-vertical-dropdown-1" variant="success">
                                    <Dropdown.Item eventKey="1" onClick={() => setModal(true)}>View</Dropdown.Item>
                                    <Modal style={{ marginTop: "150px" }} show={modalIsopen}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Infomation</Modal.Title>
                                        </Modal.Header>

                                        <Modal.Body>
                                            <div>
                                                <span style={{ float: "right", marginRight: "20px" }}><p style={{ marginTop: "10px" }}>Job: </p>
                                                    <ul>
                                                        <li>{item.job}</li>
                                                    </ul></span>
                                                <h5>{item.name}</h5>
                                                <p>{item.email}</p>
                                                <p>{item.gender}</p>
                                            </div>
                                        </Modal.Body>

                                        <Modal.Footer>
                                            <Button onClick={() => setModal(false)} variant="primary">Close</Button>
                                        </Modal.Footer>
                                    </Modal>
                                    <Dropdown.Item eventKey="2">Update</Dropdown.Item>
                                    <Dropdown.Item eventKey="3" onClick={() => onDelete(index)}>Delete</Dropdown.Item>
                                </DropdownButton>
                            </Card.Header>
                            <Card.Body>
                                <Card.Text>
                                    <h5>{item.name}</h5>
                                    <div>
                                        <p style={{ marginTop: "10px" }}>Job: </p>
                                        <ul>
                                            <li>{item.job}</li>
                                        </ul>
                                    </div>

                                </Card.Text>
                            </Card.Body>
                            <Card.Footer style={{ textAlign: "center" }} className="text-muted"><Moment fromNow locale="km">{item.submitTime}</Moment></Card.Footer>
                        </Card>))
                    }
                </Col>
            </Row>
        </Container>
    )
}
