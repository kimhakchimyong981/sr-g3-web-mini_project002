import React, { useState } from 'react'
import { Table, Col, Button, Modal } from 'react-bootstrap'
import Moment from 'react-moment';
export default function MyTable({ person, onDelete }) {
    const [modalIsopen, setModal] = useState(false)
    return (
        <Table striped bordered hover mx-5 style={{ marginTop: "10px" }}>
            <thead>
                <tr style={{ textAlign: 'center' }}>
                    <th>#</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Job</th>
                    <th>Create At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {person.map((item, index) => (
                    <tr>
                        <td key={index}>{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.gender}</td>
                        <td>{item.email}</td>
                        <td>{item.job}</td>
                        <td><Moment fromNow locale="km">{item.submitTime}</Moment></td>
                        <td><Moment fromNow locale="km">{item.submitTime}</Moment></td>
                        <td style={{ textAlign: "center" }}>
                            <Button style={{ marginLeft: "5px" }} onClick={() => setModal(true)} variant="warning" type="button">
                                View
                                </Button>
                            <Modal style={{ marginTop: "150px" }} show={modalIsopen}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Infomation</Modal.Title>
                                </Modal.Header>

                                <Modal.Body>
                                    <div>
                                        <span style={{ float: "right", marginRight: "20px" }}><p style={{ marginTop: "10px" }}>Job: </p>
                                            <ul>
                                                <li>{item.job}</li>
                                            </ul></span>
                                        <h5>{item.name}</h5>
                                        <p>{item.email}</p>
                                        <p>{item.gender}</p>
                                    </div>
                                </Modal.Body>

                                <Modal.Footer>
                                    <Button onClick={() => setModal(false)} variant="primary">Close</Button>
                                </Modal.Footer>
                            </Modal>
                            <Button style={{ marginLeft: "5px" }} variant="primary" type="button">
                                Update
                                </Button>
                            <Button style={{ marginLeft: "5px" }} onClick={() => onDelete(index)} variant="danger" type="button">
                                Delete
                                </Button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    )
}