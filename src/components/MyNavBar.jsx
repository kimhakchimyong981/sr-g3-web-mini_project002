import React from 'react'
import { Container, Navbar } from 'react-bootstrap'

function NavMenu() {
    return (
        <>
        <Navbar bg="dark" variant="dark">
            <Container>
            <Navbar.Brand href="#home">
                    <img
                        alt=""
                        src="https://st2.depositphotos.com/1064024/10769/i/600/depositphotos_107694484-stock-photo-little-boy.jpg"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}
                    Person Info React Bootstrap
                </Navbar.Brand>
            </Container>
        </Navbar>
        </>
    )
}

export default NavMenu
