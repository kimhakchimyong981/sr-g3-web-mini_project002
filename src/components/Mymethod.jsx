import { useState } from "react"

export const useForm = (init) => {
    const [value, handletext] = useState(init)

    return [value, (e) => {
        let name = e.target.name
        let namevalue = e.target.value
        let email = e.target.name
        let emailvalue = e.target.value
        handletext(crr => ({
            ...crr,
            [e.target.name]: e.target.value
        }))
        if (name === "name") {
            let pattern = /^[a-z][a-z\s]*$/i
            let result = pattern.test(namevalue.trim());
            if (result) {
                value.namerr = ""
            } else if (namevalue === "") {
                value.namerr = "*Username cannot be empty"
            } else {
                value.namerr = "*Username is invalid"
            }
        }
        if (email === "email") {
            let pattern = /^\S+@\S+\.[a-z]{3}$/g;
            let result = pattern.test(emailvalue.trim());
            if (result) {
                value.emailerr = ""
            } else if (emailvalue === "") {
                value.emailerr = "*Email cannot be empty"
            } else {
                value.emailerr = "*Email is Invalid"
            }
        }
    }]
}