import React, { useState } from 'react'
import { Component, useEffect } from 'react'
import { Form, Button, ButtonGroup, Row, Col, Container } from 'react-bootstrap'
import { useForm } from './Mymethod'
import 'moment/locale/km';
export default function MyForm({ person, test, insertperson }) {
    console.log("test method", test);
    console.log("person in from", person.length);
    const [personform, onHandleText] = useForm({
        id: "",
        name: "",
        email: "",
        namerr: "",
        emailerr: "",
        gender: "",
        job: "",
    })

    const btnvalidate = () => {
        if (personform.emailerr === "" && personform.namerr === "" && personform.email !== "" && personform.name !== "" && personform.gender !== "" && personform.job !== "") {
            return false
        } else
            return true
    }

    const onSubmitbutton = (e) => {
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < 3; i++) {
            personform.id += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        e.preventDefault();
        insertperson(personform);
    }

    const onClear = () => {

    }

    useEffect(() => {
        //     console.log("id",personform.id,"name",personform.name,personform.email);
        //     console.log("last update",person);
        // if(personform.email==="narath@gmail.com"){
        //     console.log("email is correct");
        // }else
        // console.log("email is incorrect");
        // console.log(personform.id,personform.name,personform.email);
        console.log("useeffect work!");
    }, [personform]);
    return (
        <Container>
            <h2>Personal Informationssss</h2>
            <Row>
                <Col>
                    <Form onSubmit={onSubmitbutton}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control name="name" onChange={onHandleText} type="text" placeholder="username" />
                            <Form.Text className="text-muted">
                                <p style={{ color: "red" }}>{personform.namerr}</p>
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" onChange={onHandleText} type="email" placeholder="email" />
                            <Form.Text className="text-muted">
                                <p style={{ color: "red" }}>{personform.emailerr}</p>
                            </Form.Text>
                        </Form.Group>
                        <div style={{ textAlign: 'right' }}>
                            <Button disabled={btnvalidate()} variant="primary" type="submit" style={{ marginRight: 3 }}>
                                Submit
                                </Button>
                            {/* <Button onClick={onClear} variant="secondary" type="button">
                                Cancel
                                </Button> */}
                            <Button variant="secondary" type="reset">clear</Button>
                        </div>
                    </Form>
                </Col>
                <Col>
                    <Form>
                        <h3>Gender</h3>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check custom inline type="radio" value="male" label="Male" name="gender" id="male" checked={personform.gender == "male"}
                                onChange={onHandleText}
                                defaultChecked={true} />
                            <Form.Check custom inline type="radio" value="female" label="Female" name="gender" checked={personform.gender === "female"}
                                onChange={onHandleText}
                            />
                        </Form.Group>
                        <h3>Job</h3>

                        {/* <input type="checkbox" name="student" label="student"/> 
                        <input type="checkbox" name="Teacher"/> Teacher
                        <input type="checkbox" name="Developer"/> Developer */}
                        <Form.Group controlId="formBasicCheckbox">
                            {/* <Form.Check custom inline label="Student" name="job" id="student" Checked={personform.job === "student"}  onChange={onHandleText}  />
                            <Form.Check custom inline label="Teacher" name="job" id="teacher" onChange={onHandleText} />
                            <Form.Check custom inline label="Developer" name="job" id="developer" onChange={onHandleText} /> */}

                            {/* mine */}
                            <Form.Check custom inline label="Student" value="student" id="student" name="job" checked={personform.job == "student"} onChange={onHandleText} />
                            <Form.Check custom inline label="Teacher" value="teacher" id="teacher" name="job" checked={personform.job == "teacher"} onChange={onHandleText} />
                            <Form.Check custom inline label="Developer" value="developer" id="developer" name="job" checked={personform.job == "developer"} onChange={onHandleText} />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}
