import React, { Component } from 'react'
import { Container, Row, Button, ButtonGroup } from 'react-bootstrap'
import MyNarBar from './components/MyNavBar'
import MyForm from './components/MyForm'
import MyTable from './components/MyTable'
import MyCard from './components/MyCard'
import MyPagination from './components/MyPagination'
import { useState } from 'react'
export default function App() {

  const [person, setPerson] = useState([{
    id: "sy2",
    name: "Virus",
    email: "Trojan@gmail.com",
    gender: "Male",
    job: "Student",
    submitTime: new Date().toISOString()
  },])
  const [show, setShow] = useState(true)
  const insertperson = (newperson) => {
    setPerson(crr => [...crr, newperson])
  }

  const test = (a) => {
    alert(a)
  }
  const onClickTable = () => {
    console.log("you are click table");
  }
  const onClickCard = () => {
    console.log("you are click card");
  }
  console.log("person", person);
  console.log("test1", test);

  const onDelete = (index) => {
    let newperson = person
    newperson.splice(index, 1)
    console.log("delete", newperson);
    setPerson([...newperson])
  }
  return (
    <div>
      <header>
        <MyNarBar />
      </header>
      <section>
        <Container>
          <MyForm person={person} insertperson={insertperson} test={test} />
          <h3>Display Data As:</h3>
          <div style={{ marginBottom: 10, marginTop: 10 }}>
            <ButtonGroup aria-label="Basic example">
              <Button onClick={() => setShow(true)} variant="secondary">Table</Button>
              <Button onClick={() => setShow(false)} variant="secondary">Card</Button>
            </ButtonGroup>
          </div>
          {
            show ? <MyTable person={person} onDelete={onDelete} /> : <MyCard person={person} onDelete={onDelete} />
          }
        </Container>
      </section>
      <footer style={{ textAlign: 'center' }}>
        <MyPagination />
      </footer>
    </div>
  )
}